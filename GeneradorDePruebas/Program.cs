﻿using System;
using System.Collections.Generic;

namespace GeneradorDePruebas
{
    class Program
    {
        public static List<PublicacionViewModel> Publicacions { get; set; }
        static void Main(string[] args)
        {
            using (var writer = new System.IO.StreamWriter(@"C:\Users\ariel.bautista\Desktop\LayoutAutomatico.txt"))
            {
                var random = new Random();
                string[] moneda = { "MXN", "USD", "EUR" };
                string[] Idcompradores = { "32541", "12578", "20054", "12354", "12365" };
                string[] Nombrescompradores = { "Axtel S.A.B. de C.V.", "Micronet de México SA DE CV",
                    "Oracle de México S.A. DE C.V.",
                    "Huawei Technologies de México SA de CV", "E Factor Diez S.A. de C.V. SOFOM E.N.R." };
                int cantidadregistros = random.Next(5, 100);

                for (int i = 0; i < cantidadregistros; i++)
                {
                    var compradoSelector = random.Next(0, 5);

                    var publicacion = new PublicacionViewModel()
                    {
                        TipoDocumento = "PO",
                        IdProveedor = "5005474445",
                        NombreProveedor = "Huawei Technologies de México SA de CV",
                        Moneda = moneda[random.Next(0, 3)],
                        IdComprador = Idcompradores[compradoSelector],
                        NombreComprador = Nombrescompradores[compradoSelector],
                        FechaEmision = DateTime.Now.AddDays(random.Next(-100, 0)).ToShortDateString(),
                        MontoDocumento = random.Next(1000, 9999999).ToString(),
                        FechaVencimientoDocumento = DateTime.Now.AddDays(random.Next(0, 500)).ToShortDateString(),
                        NumeroDocumentoNotaCredito = random.Next(10000, 99999).ToString(),
                        IdenticadorUnicoDocumento = random.Next(10000000, 99999999).ToString(),
                        FechaAutoDescuento = DateTime.Now.AddDays(random.Next(0, 500)).ToShortDateString(),
                        CantidadRegistroDetalle = random.Next(10, 9999).ToString(),
                        UUDI = random.Next(1000000, 9999999).ToString()
                    };

                    writer.WriteLine(publicacion.ToString);

                    Console.WriteLine(publicacion.ToString);
                }
                writer.Close();
            }
        }
    }
    public class PublicacionViewModel
    {
        public string TipoDocumento { get; set; }
        public string IdComprador { get; set; }
        public string NombreComprador { get; set; }
        public string Moneda { get; set; }
        public string IdProveedor { get; set; }
        public string NombreProveedor { get; set; }
        public string FechaEmision { get; set; }
        public string MontoDocumento { get; set; }
        public string FechaVencimientoDocumento { get; set; }
        public string NumeroDocumentoNotaCredito { get; set; }
        public string IdenticadorUnicoDocumento { get; set; }
        public string FechaAutoDescuento { get; set; }
        public string CantidadRegistroDetalle { get; set; }
        public string UUDI { get; set; }
        public new string ToString => TipoDocumento + "|" + IdProveedor + "|" + NombreProveedor + "|" + Moneda + "|" + IdComprador + "|" + NombreComprador + "|" + FechaEmision + "|" + MontoDocumento + "|" + FechaVencimientoDocumento + "|" + NumeroDocumentoNotaCredito + "|" + IdenticadorUnicoDocumento + "|" + FechaAutoDescuento + "|" + CantidadRegistroDetalle + "|" + UUDI + "|";
    }
}
